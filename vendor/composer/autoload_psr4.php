<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'yii\\swiftmailer\\' => array($vendorDir . '/yiisoft/yii2-swiftmailer'),
    'yii\\gii\\' => array($vendorDir . '/yiisoft/yii2-gii'),
    'yii\\debug\\' => array($vendorDir . '/yiisoft/yii2-debug'),
    'yii\\composer\\' => array($vendorDir . '/yiisoft/yii2-composer'),
    'yii\\bootstrap\\' => array($vendorDir . '/yiisoft/yii2-bootstrap'),
    'yii\\authclient\\' => array($vendorDir . '/yiisoft/yii2-authclient'),
    'yii\\' => array($vendorDir . '/yiisoft/yii2'),
    'kop\\y2sp\\' => array($vendorDir . '/kop/yii2-scroll-pager'),
    'cebe\\markdown\\' => array($vendorDir . '/cebe/markdown'),
);
